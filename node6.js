function hello(asdd){
    console.log(asdd);
}

var async_function = function(val, callback){
    process.nextTick(function(){
        callback(val);
    });
};

async function vivek(data){
    console.log('asynchronous');
    await console.log('await is here '+data+12);
    console.log('Now I am complete');
    return data;
}


async_function(67,hello);
console.log(vivek(11))
let call_vivek=vivek(1221);
console.log(call_vivek);
console.log('before async');
call_vivek.then(data=>console.log(data))
console.log('after async');