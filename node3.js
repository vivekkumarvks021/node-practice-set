var events = require('events');

var em = new events.EventEmitter();

em.on('MyFirstEvent', function (data) {
    console.log('Hello Sir: ' + data);
});

em.emit('MyFirstEvent', 'This is my first Node.js event emitter example.');
setTimeout(function(req,res){
    em.emit('MyFirstEvent', 'This is my second Node.js event emitter example.');
},900000);